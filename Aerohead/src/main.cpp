#define VERSION_STRING "V1.0.4"

#include <Arduino.h>
#include <string>
#include <SPI.h>
#include <TimeLib.h>

#include <sdpsensor.h>
#include <SparkFunBME280.h>
#include <TinyGPS++.h>

#include <message.h>

#ifndef SERIAL3_TX_BUFFER_SIZE
#define SERIAL3_TX_BUFFER_SIZE 512 // number of outgoing bytes to buffer
#endif

#define CHIP_SELECT 1 // 1 for new boards.  9 for the original.

#define SCLK 13
#define MOSI 11
#define MISO 12

#define LED_RED 16
#define LED_GREEN 17

#define IR_IN 22
#define ODOMETER_IN 2

#define REDUNDANT_ODO_IN A12
#define NOTUSED_UART_IN 0

#define DEBUG_SERIAL_INTERRUPT_IN 6
#define DEBUG_SERIAL_STATUS_PIN 4

TinyGPSPlus gps;

SDPSensor pitotSensorA = SDPSensor(0x25);
SDPSensor pitotSensorB = SDPSensor(0x25);
BME280 environmentSensorA;
BME280 environmentSensorB;

const uint32_t MaxUint32 = 0xffffffff;

// RTC
uint32_t clockOffset;
// uint32_t rtcOffset;

// IR Receiver
const int window = 50;
volatile unsigned long lastIRTick = 0;
// volatile unsigned long lastIRPulse = 0;
volatile unsigned long irPulseCount = 0;

const unsigned int IR_THRESHOLD = 4;
volatile unsigned long irTimeStamp = 0;
volatile unsigned int irPulseStartTime = 0;
volatile unsigned int irLapCount;
volatile unsigned long irTimeStampOld = 0;
unsigned long irCountForMessage = 0;

// volatile bool lastIRValid = false;
volatile unsigned long lastPulseLength = 0;

// odometer
volatile unsigned long odometerTick = 0;
volatile unsigned long odometerCount = 0, epochOdometerCount = 0;
volatile unsigned long lastOdometerCount = 0, epochOdometerTick = 0;
volatile unsigned long odoInterval0 = 0, odoInterval1 = 0;
volatile unsigned long epochOdoInterval0 = 0, epochOdoInterval1 = 0;

volatile bool OdoLedActive = true;

float CalculateRho(float temperatureCelcius, float relativeHumidity, float rawPressure);
uint32_t rtcMillis();
uint32_t rtcMicros();
void HandleIR();
void OdometerHandler();
u_int32_t DateStamp();
u_int32_t TimeStamp();

static void WriteGPSConfig(const uint8_t message[], uint8_t length)
{
  Serial3.write(message, length);
}
static void Set115kbaud()
{
  const uint8_t Set115kbaud[] = {0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x00, 0xC2, 0x01, 0x00, 0x07, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x7E};

  WriteGPSConfig(Set115kbaud, 28);
}
static void ConfigureGPS()
{
  const uint8_t message05Hz[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xC8, 0x00, 0x01, 0x00, 0x01, 0x00, 0xDE, 0x6A};
  const uint8_t message10Hz[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0x64, 0x00, 0x01, 0x00, 0x00, 0x00, 0x79, 0x10};
  const uint8_t messageNmeaOutOnly[] = {0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x00, 0xC2, 0x01, 0x00, 0x07, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBF, 0x78};
  const uint8_t messageDisableMessage1[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x01, 0x00, 0xFB, 0x11};
  const uint8_t messageDisableMessage2[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x02, 0x00, 0xFC, 0x13};
  const uint8_t messageDisableMessage3[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x03, 0x00, 0xFD, 0x15};
  const uint8_t messageDisableMessageGSV[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x03, 0x00, 0xFD, 0x15};
  const uint8_t messageEnableGGA[] = {0xB5, 0x62, 0x05, 0x01, 0x02, 0x00, 0x06, 0x01, 0x0F, 0x38};

  // WriteGPSConfig(message05Hz, 14);
  // delay(10);
  WriteGPSConfig(messageDisableMessage3, 11);
  delay(10);

  WriteGPSConfig(messageNmeaOutOnly, 28);
  delay(10);
  WriteGPSConfig(messageDisableMessage1, 11);
  delay(10);
  WriteGPSConfig(messageDisableMessage2, 11);
  delay(10);

  WriteGPSConfig(message05Hz, 14);
  delay(10);
}

volatile uint32_t chipSelectEpoch = 0;
volatile uint32_t gpsEpoch = 0;

static void FeedGPS()
{

  while (Serial3.available())
  {
    // delayMicroseconds(10);
    // digitalWriteFast(DEBUG_SERIAL_STATUS_PIN, HIGH);
    gps.encode(Serial3.read());
    // digitalWriteFast(DEBUG_SERIAL_STATUS_PIN, LOW);
  }
  //
}

void serialEvent3()
{
  FeedGPS();
}

void GpsEpochHandler()
{
  if (gpsEpoch > 0)
    return;
  gpsEpoch = micros();
}

void ChipSelectHandler()
{

  uint32_t epoch = micros();

  if (chipSelectEpoch > 0)
    return;

  noInterrupts();
  chipSelectEpoch = epoch;
  // Snapshop copy odometer variables
  epochOdometerCount = odometerCount;
  epochOdometerTick = odometerTick;
  epochOdoInterval0 = odoInterval0;
  epochOdoInterval1 = odoInterval1;
  interrupts();
}

u_int32_t DateStamp(const uint16_t year, const uint8_t month, const uint8_t day)
{
  return (year * 10000) + (month * 100) + day;
}

u_int32_t TimeStamp(const uint8_t hour, const uint8_t minute, const uint8_t second, uint8_t centisecond)
{
  return (hour * 10000000) + (minute * 100000) + (second * 1000) + (centisecond * 10);
}

static void Measure()
{
  uint32_t pitotReadTimeOut = chipSelectEpoch + 205000; // gpsEpoch + 250000; //185500; //fine tune to maximise free running samples and still allow SPI comms
  if (chipSelectEpoch == 0)
  {
    pitotReadTimeOut = micros() + 210000;
  }

  // read environment sensor as this is one shot and only takes time from pitot samples
  float tempA = environmentSensorA.readTempC();
  float pressureA = environmentSensorA.readFloatPressure();
  float humidityA = environmentSensorA.readFloatHumidity();

  float tempB = environmentSensorB.readTempC();
  float pressureB = environmentSensorB.readFloatPressure();
  float humidityB = environmentSensorB.readFloatHumidity();

  //allow for failed sensors
  if (pressureA<85000){
      pressureA = pressureB;
  } 
  else
  {
    if (pressureB<85000) pressureB = pressureA;
  }
  

  float pitotPressurePaA = 0.0f; //= pitotSensorA.getDifferentialPressure();
  unsigned long samplesA = 0;

  float pitotPressurePaB = 0.0f; //= pitotSensorA.getDifferentialPressure();
  unsigned long samplesB = 0;

  if (humidityA == 0.0)
    humidityA = 50.0; // arbitrary value;

  if (humidityB == 0.0)
    humidityB = 50.0;

  do
  { // READ PITOT (at least every 10ms, max 0.5ms/2kHz)

    uint8_t status = pitotSensorA.readSample();
    if (status == 0)
    {
      double sample = pitotSensorA.getDifferentialPressure();
      pitotPressurePaA += sample;
      samplesA++;
    }

    for (int n = 0; n < 5; n++)
    {

      if (chipSelectEpoch > 0)
        break;
      delayMicroseconds(1);
    }
    if (chipSelectEpoch > 0)
      break;

    status = pitotSensorB.readSample();
    if (status == 0)
    {
      double sample = pitotSensorB.getDifferentialPressure();
      pitotPressurePaB += sample;
      samplesB++;
    }

    for (int n = 0; n < 5; n++)
    {

      if (chipSelectEpoch > 0)
        break;

      delayMicroseconds(1);
    }
    if (chipSelectEpoch > 0)
      break;

    analogWrite(A14, max(0, (long)(pitotPressurePaA * PitotScale / samplesA))); // VERY DEBUG

  } while ((chipSelectEpoch == 0) && (micros() <= pitotReadTimeOut));

  // interpolate odometer
  noInterrupts();
  //uint32_t chipSelectEpochTemp = chipSelectEpoch;
  uint32_t odometerCountTemp = epochOdometerCount;
  uint32_t odoInterval0Temp = epochOdoInterval0;
  uint32_t odoInterval1Temp = epochOdoInterval1;
  uint32_t odometerTickTemp = epochOdometerTick;
  interrupts();

  uint32_t timeSinceLastOdometerEvent = (chipSelectEpoch >= odometerTickTemp) ? (chipSelectEpoch - odometerTickTemp) :0; // (MaxUint32 - chipSelectEpochTemp) + odometerTickTemp + 1;

  double predictedOdoInterval = ((double)odoInterval1Temp * 2.0) - (double)odoInterval0Temp;
  // float predictedOdoInterval = (float)(odoInterval1Temp*2) - (float)odoInterval0Temp;
  double partialOdometerCount = fmin((double)timeSinceLastOdometerEvent / predictedOdoInterval, 1.0);
  partialOdometerCount = fmax(partialOdometerCount,0);
  double totalOdometerCount = (double)odometerCountTemp + partialOdometerCount;

  digitalWriteFast(MISO, 1); // DEBUGGING timing

  if (chipSelectEpoch == 0)
  {
    Serial.printf("NO SPI CS\n");
  }

  float pitotTemperatureA = pitotSensorA.getTemperature();
  // float pitotTemperatureB = pitotSensorB.getTemperature();
  // Serial.printf("P1: %0.4fPa %0.2fdeg / %d \n", pitotPressurePaA, pitotTemperatureA, samplesA);
  // Serial.printf("P2: %0.4fPa %0.2fdeg / %d \n", pitotPressurePaB, pitotTemperatureB, samplesB);
  if (samplesA > 1)
  {
    pitotPressurePaA = pitotPressurePaA / (double)samplesA;
  }
  if (samplesB > 1)
    pitotPressurePaB = pitotPressurePaB / (double)samplesB;

  if (gps.time.isValid() && gps.date.year() > 2020 && gps.time.centisecond() == 0 && gps.time.second() == 0)
  {
    TimeElements tm;
    tm.Day = gps.date.day();
    tm.Month = gps.date.month();
    tm.Year = gps.date.year() - 1970;
    tm.Hour = gps.time.hour();
    tm.Minute = gps.time.minute();
    tm.Second = gps.time.second();

    time_t time = makeTime(tm);

    Teensy3Clock.set(time);
    setTime(tm.Hour, tm.Minute, tm.Second, tm.Day, tm.Month, tm.Year);
    // Serial.printf("RTC SYNC\n");
  }

  Serial.printf("SAMPLES: %d %d, ", samplesA, samplesB);
  // Serial.printf("RAW, %0d, %0d, %0d, %0d, %0.3f\n", chipSelectEpoch, millis(), epochOdoInterval1, odometerTickTemp, totalOdometerCount);
  Serial.printf(" RTC: %d, GPS: %d", TimeStamp(hour(), minute(), second(), 0), TimeStamp(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.time.centisecond()));
  Serial.printf(" [%s%s], V:%0.3fm/s", gps.location.isValid() ? "V" : "_", gps.location.isUpdated() ? "U" : "_", gps.speed.mps());
  Serial.printf(" %0.3fPa,  %0.3fPaB, %0.2fTA, %0.2fODO", pitotPressurePaA, pitotPressurePaB, pitotTemperatureA, totalOdometerCount);
  Serial.printf(" %0.0fAtmMu, ", (pressureA+pressureB)/2.0);
  Serial.printf(" %0.1fRHA, %0.0fRHB", humidityA, humidityB);
  // Serial.printf(" HDOP: %d", gps.hdop.value());
  // Serial.printf(" ERROR: %d\n", gps.failedChecksum());
  Serial.println();

  if (irTimeStamp > irTimeStampOld) // && (chipSelectEpoch/1000) >= irTimeStamp)
  {
    irCountForMessage = ((irLapCount & 0xff) << 24) + ((irTimeStamp - irTimeStampOld) & 0xffffff);
    irTimeStampOld = irTimeStamp;
    Serial.printf("%d, %d, %d\n", millis(), irCountForMessage & 0xffffff, irCountForMessage >> 24);
  }

  AeroheadMessage msg;
  msg.Counter = chipSelectEpoch;
  msg.Offset = timeSinceLastOdometerEvent; // chipSelectEpoch > gpsEpoch ? chipSelectEpoch - gpsEpoch : 0;
  gpsEpoch = 0;

  if (gps.location.isValid())
  {

    // Serial.println("VALID GPS");
    msg.HDOP = (int)min(255, gps.hdop.value());
    msg.Altitude = (int32_t)(gps.altitude.meters() * (double)AltitudeScale);
    msg.Lat = (int32_t)(gps.location.lat() * (double)LatScale);
    msg.Lon = (int32_t)(gps.location.lng() * (double)LonScale);
    msg.Speed = (int32_t)(gps.speed.mps() * GpsSpeedScale);
    msg.DateStamp = DateStamp(gps.date.year(), gps.date.month(), gps.date.day());
    msg.TimeStamp = TimeStamp(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.time.centisecond());
  }
  else
  {
    // Serial.println("NO GPS");
    msg.HDOP = 0;
    msg.Altitude = 0;
    msg.Lat = 0;
    msg.Lon = 0;
    msg.Speed = 0;
    msg.DateStamp = DateStamp(year(), month(), day());
    msg.TimeStamp = TimeStamp(hour(), minute(), second(), 0);
  }

  msg.PitotVelocityPressure =(long)(pitotPressurePaA * PitotScale);
  msg.PitotAnglePressure = (long)(pitotPressurePaB * PitotScale);
  msg.PressureAtmosphere = (long)((pressureA + pressureB) * AtmPressureScale) / 2;
  msg.RelativeHumidity = (long)(humidityA * HumidityScale) / 1;
  msg.Temperature = (long)(pitotTemperatureA * TemperatureScale);
  msg.InfraredCount = irCountForMessage; // 2 MSBytes = lap count
  msg.OdometerCount = totalOdometerCount * OdoScale;
  msg.OdometerSpeed = odoInterval1Temp;

  // Serial.printf("MSG, %0d, %0d\n", msg.PitotAnglePressure, msg.OdometerCount);

  if (gps.time.centisecond() == 0)
    digitalWriteFast(gps.location.isValid() ? LED_GREEN : LED_RED, 1);

  digitalWriteFast(MISO, LOW); // DEBUGGING timing

  chipSelectEpoch = 0;
  bool ok = 1;

  unsigned char *SpiTransmissionBuffer = (unsigned char *)malloc(sizeof(AeroheadMessage));
  memcpy(SpiTransmissionBuffer, (const unsigned char *)&msg, sizeof(AeroheadMessage));

  uint32_t commsTimeOut = millis() + 100;

  // write up to length of buffer - master clocks data out.
  for (unsigned int bytePosition = 0; bytePosition < sizeof(msg); bytePosition++)
  {

    uint8_t txdata = SpiTransmissionBuffer[bytePosition];

    for (int bitPosition = 0; bitPosition < 8; bitPosition++) //  for (i = 0; i < NBITS && ok; i++)
    {

      // write bits 7..0 (MS bit first).
      digitalWriteFast(MISO, (txdata & 0x80) > 0);

      while ((digitalReadFast(SCLK) == 0) && ok)
      { /* Wait for clock high */

        if (digitalReadFast(CHIP_SELECT) )
        {
          ok = false;
        }
      }

      while ((digitalReadFast(SCLK) != 0) && ok)
      { /* Wait for clock low */

        if (digitalReadFast(CHIP_SELECT) )
        {
          ok = false;
        }
      }

      if (!ok)
        break;

      txdata = txdata << 1;
    }
    if (!ok)
      break;
  }

  free(SpiTransmissionBuffer);

  digitalWriteFast(gps.location.isValid() ? LED_GREEN : LED_RED, 0);

  digitalWriteFast(MISO, 0);
}

time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}

void setup()
{
  // RTC SYNC
  setSyncProvider(getTeensy3Time);
  // RTC INTERRUPTS
  RTC_IER |= 0x10; // set the TSIE bit (Time Seconds Interrupt Enable)
  NVIC_ENABLE_IRQ(IRQ_RTC_SECOND);

  pinMode(A14, OUTPUT);
  analogWriteResolution(12);

  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(NOTUSED_UART_IN, INPUT);

  digitalWriteFast(LED_RED, HIGH);
  delay(250);
  Serial.begin(9600);
  digitalWriteFast(LED_RED, LOW);
  digitalWriteFast(LED_GREEN, HIGH);

  delay(250);
  digitalWriteFast(LED_GREEN, LOW);

  TimeElements tm;
  tm.Day = day();
  tm.Month = month();
  tm.Year = year() - 1970;
  tm.Hour = hour();
  tm.Minute = minute();
  tm.Second = second();

  time_t time = makeTime(tm);

  Teensy3Clock.set(time);                                           // set the RTC
  setTime(tm.Hour, tm.Minute, tm.Second, tm.Day, tm.Month, year()); // set system clock

  Wire.begin();
  Wire.setClock(400000);
  delay(5);
  Wire1.begin();
  Wire1.setClock(400000);

  delay(250);

  Serial.printf("Version %s\n", VERSION_STRING);
  Serial.printf("SPI Comms CS on IO%d\n", CHIP_SELECT);

  uint8_t status = pitotSensorA.init(&Wire);
  // if (status != 0)
  //{
  Serial.printf("PITOT A STARTUP: %s (%d).\n", (status == 1) ? "OK" : "FAIL", status);
  //}

  status = pitotSensorB.init(&Wire1);
  // if (status != 0)
  //{
  Serial.printf("PITOT B STARTUP: %s (%d).\n", (status == 1) ? "OK" : "FAIL", status);
  //}

  environmentSensorA.settings.commInterface = I2C_MODE;
  environmentSensorA.settings.I2CAddress = 0x76; // default
  environmentSensorA.settings.runMode = 3;       // Normal mode

  environmentSensorA.settings.tStandby = 0;
  environmentSensorA.settings.filter = 4;
  environmentSensorA.settings.tempOverSample = 5;
  environmentSensorA.settings.pressOverSample = 4;
  environmentSensorA.settings.humidOverSample = 2;

  environmentSensorB.settings.commInterface = I2C_MODE;
  environmentSensorB.settings.I2CAddress = 0x76; // default
  environmentSensorB.settings.runMode = 3;       // Normal mode

  environmentSensorB.settings.tStandby = 7;
  environmentSensorB.settings.filter = 16;
  environmentSensorB.settings.tempOverSample = 5;
  environmentSensorB.settings.pressOverSample = 4;
  environmentSensorB.settings.humidOverSample = 2;

  status = environmentSensorA.beginI2C(Wire);
  // if (status != 0)
  //{
  Serial.printf("BMx280 A STARTUP: %s (%d).\n", (status == 1) ? "OK" : "FAIL", status);

  //}

  status = environmentSensorB.beginI2C(Wire1);

  // if (status != 0)
  // {
  Serial.printf("BMx280 B STARTUP: %s (%d).\n", (status == 1) ? "OK" : "FAIL", status);

  // }

  pinMode(CHIP_SELECT, INPUT);
  pinMode(SCLK, INPUT);
  pinMode(MOSI, INPUT);
  pinMode(MISO, OUTPUT);

  digitalWriteFast(MISO, LOW);

  attachInterrupt(CHIP_SELECT, ChipSelectHandler, FALLING);

  delay(250);
  digitalWriteFast(LED_RED, HIGH);
  delay(250);
  digitalWriteFast(LED_GREEN, LOW);

  // GPS

  Serial3.begin(9600);
  delay(10);
  ConfigureGPS();
  delay(10);

  Set115kbaud();
  delay(10);

  Serial3.end();
  delay(10);

  Serial3.begin(115200);
  delay(10);
  ConfigureGPS();
  delay(10);

  pinMode(DEBUG_SERIAL_INTERRUPT_IN, INPUT);
  pinMode(DEBUG_SERIAL_STATUS_PIN, OUTPUT);

  attachInterrupt(DEBUG_SERIAL_INTERRUPT_IN, GpsEpochHandler, FALLING);

  //  Serial3.end();     //DEBUG ONLY
  //  pinMode(7, INPUT); //DEBUG ONLY
  //  pinMode(8, INPUT); //DEBUG ONLY

  // INFRARED RECEIVER
  attachInterrupt(IR_IN, HandleIR, CHANGE); // IR Marker

  // ODOMETER
  pinMode(ODOMETER_IN, INPUT);
  pinMode(REDUNDANT_ODO_IN, INPUT);                       // REMOVE ONCE PCB FIXED
  attachInterrupt(ODOMETER_IN, OdometerHandler, FALLING); // Odometer

  Serial.printf("GPS Serial Buffer size: %d\n", SERIAL3_TX_BUFFER_SIZE);

  // ZEROS FOR CLOCKS
  clockOffset = millis();
}

void loop()
{
  // Serial.println("LOOP");
  Measure();
}

float CalculateRho(float temperatureCelcius, float relativeHumidity, float rawPressure)
{
  float temperatureKelvin = temperatureCelcius + 273.15;
  float saturationPressure = 6.0178 * pow(10, ((7.5 * temperatureKelvin) - 2048.625) / (temperatureKelvin - 35.85));
  float vapourPressure = (relativeHumidity * saturationPressure) / 100.0;
  float dryPartialPressure = (rawPressure - vapourPressure) / (287.058 * temperatureKelvin);
  float vapourPartialPressure = vapourPressure / (461.495 * (temperatureKelvin));
  return (dryPartialPressure + vapourPartialPressure) * 100.0;
}

volatile unsigned long IRDebounce = 0;

void HandleIR()
{

  bool irState = digitalReadFast(IR_IN);
  unsigned long epoch = micros();

  if (epoch < IRDebounce)
  {
    IRDebounce = epoch + 1000000;
    return;
  }
  long interval = epoch - lastIRTick;
  lastIRTick = epoch;

  if (irPulseStartTime == 0)
  { // flag start of pulses
    irPulseStartTime = millis();
    irPulseCount = 1;

    // Serial.print(" START ");
  }

  long pulseLength = 4000 - lastPulseLength;
  lastPulseLength = interval;

  if (irState)
  {
    if (pulseLength < 400 || pulseLength > 600)
    {
      pulseLength = 505;
    }
  }
  else
  {
    if (pulseLength < 3200 || pulseLength > 3600)
    {
      pulseLength = 3495;
    }
  }

  if (interval > pulseLength - window && interval < pulseLength + window)
  {

    /// Serial.print(" X ");

    irPulseCount++;

    if (irPulseCount > IR_THRESHOLD)
    {
      IRDebounce = epoch + 4900000; // 5s
      // lastIRPulse = millis() - clockOffset;
      irTimeStamp = irPulseStartTime;
      irPulseStartTime = 0;
      irLapCount++;
    }

    return;
  }
  irPulseStartTime = 0;
  irPulseCount = 0;
}

volatile uint32_t lastOdoEvent = 0;

void OdometerHandler()
{
  uint32_t now = micros();

  if (now < lastOdoEvent + 60000)
  { // debounce 16 rev/s max 2m circumference approx 32m/s 65mph
    lastOdoEvent = now;
    return;
  }
  noInterrupts();
  lastOdoEvent = now;

  odoInterval0 = odoInterval1;
  // odoInterval1 = (now > odometerTick) ? now - odometerTick : (MaxUint32 - odometerTick) + now + 1;
  odoInterval1 = now - odometerTick;
  odometerTick = now;
  odometerCount++;
  interrupts();
 
}

void rtc_seconds_isr(void) // RTC Epoch ISR
{
  // rtcEpoch = rtcMillis();
}

//  combine RTC TPR with seconds
// https://community.nxp.com/thread/378715
// double reads insure consistent
uint32_t rtcMillis()
{
  return rtcMicros() / 1000;
}

uint32_t rtcMicros()
{
  uint32_t read1, read2, secs, us;

  do
  {
    read1 = RTC_TSR;
    read2 = RTC_TSR;
  } while (read1 != read2); // insure the same read twice to avoid 'glitches'
  secs = read1;
  do
  {
    read1 = RTC_TPR;
    read2 = RTC_TPR;
  } while (read1 != read2);                                    // ensure the same read twice to avoid 'glitches'
                                                               // Scale 32.768KHz to microseconds, but do the math within 32bits by leaving out 2^6
                                                               //  30.51758us per TPR count
  us = (read1 * (1000000UL / 64) + 16384 / 64) / (32768 / 64); // Round crystal counts to microseconds
  if (us < 100)                                                // if prescaler just rolled over from zero, might have just incremented seconds -- refetch
  {
    do
    {
      read1 = RTC_TSR;
      read2 = RTC_TSR;
    } while (read1 != read2); // ensure the same read twice to avoid 'glitches'
    secs = read1;
  }
  return (secs * 1000000) + us; // us
}
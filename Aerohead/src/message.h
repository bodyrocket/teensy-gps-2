#ifdef ARDUINO
#define u_int8_t unsigned char
#define u_int32_t unsigned long
#define int32_t long
#define int8_t short
#endif

const int32_t LatScale = 10000000;
const int32_t LonScale = 10000000;
const int32_t AltitudeScale = 100;
const int32_t HdopScale = 100;
const int32_t GpsSpeedScale = 100;
const int32_t TemperatureScale = 10;
const int32_t PitotScale = 1000;
const int32_t AtmPressureScale = 10;
const int32_t HumidityScale = 10;
const int32_t OdoScale = 1000;

struct AeroheadMessage //struct used for byte encoding/decoding over SPI.
{
    //SYSTEM
    u_int32_t Offset;
    u_int32_t Counter;

    //DATETIME
    u_int32_t DateStamp; // ISO ish e.g. 20210517 - 17 May 2021
    u_int32_t TimeStamp; // ISO ish  with ms e.g. 12570023 - 12:57:00.23

    //GPS
    int32_t Lat;
    int32_t Lon;
    int32_t Altitude;
    u_int32_t Speed;
    u_int8_t HDOP; //scale 100

    //PITOT
    int32_t PitotVelocityPressure;
    int32_t PitotAnglePressure;

    //ENVIRONMENT
    int32_t Temperature;
    u_int32_t RelativeHumidity;
    u_int32_t PressureAtmosphere;

    //INFRARED
    u_int32_t InfraredCount;

    //ODOMETER
    u_int32_t OdometerCount;
    u_int32_t OdometerSpeed;
};
